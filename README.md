File structure inspired by documentation in https://github.com/puikinsh/Adminator-admin-dashboard

### File Structure

```
|_ README.md                # this file
|_ redticket.js             # routing configuration of app
|_ settings.js              # storage of any global parameters that need to be set application-wide
|_ controllers              # folder containing all controllers referenced in redticket.js
|   |_ admin.js
|   |_ ajax.js
|   |_ clients.js
|   |_ engineers.js
|   |_ main.js
|   |_ tickets.js
|
|_ models                   # folder containing all models and connections to databases referenced in controllers files
|   |_ connections.js       # stores connection information for db - used to switch between testing and live
|   |_ customers_tickets.js # customer-to-tickets information model
|   |_ customers.js         # customer information model
|   |_ db.js                # defines relationship between all database models
|   |_ roles.js
|   |_ tickets.js
|   |_ users.js
|
|_ public                   # path to all static material/assets referenced in redticket.js and views
|   |_ ..                   # assets imported from https://github.com/puikinsh/sufee-admin-dashboard
|
|_ views                    # folder containing all views used by controllers - using PUG engine
|   |_ admin.pug            # view for admin page
|   |_ clients.pug          # view to display all clients
|   |_ engineer.pug         # view for engineer page
|   |_ engineers.pug        # view to display all engineers
|   |_ home.pug             # view for home/landing page
|   |_ login.pug            # view for login page
|   |_ search.pug           # view for search Results
|   |_ ticket.pug           # view for ticket page
|   |_ tickets.pug          # view to display all tickets
|   |_ common               # folder containing all common views imported in other views
|       |_ footer.pug       # view for footer that is imported into other views
|       |_ header.pug       # view for header that is imported into other views
|       |_ nav.pug          # view for navbar that extends header
```
### Outstanding Tasks

* [ ] Include package.js
* [ ] Include instructions on installing/running application 
* [ ] Fix .badge-pill and .badge-success not working
* [ ] Improve file structure
  * [ ] Main content must extend header, so header must extend nav - can we extend multiple sources, or import multiple pug files?
* [ ] Decide between importing using require or import (https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export)
* [ ] Complete writing description for file structure
* [ ] Change app name officially to Cricket
