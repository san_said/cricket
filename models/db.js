'use strict'

var sql = require('sequelize'),
    config = require(__dirname + "/../settings");

if (!config.testing) {
  console.log("Please configure a live setup");
  return;
};

var connection = new sql('Test_RedTicket', 'postgres', 'rabiosa8393', {
  host: 'localhost',
  port: 5433,
  dialect: 'postgres',
  define: {timestamps: false},
  pool: {
    max: 10,
    min: 5,
    idle: 10000
  },
  omitNull: true,
  dialectOptions: {
    ssl: false // reconfigure your psql server so that it accepts SSL - instructions: https://stackoverflow.com/questions/38134557/having-trouble-setting-up-postgres-server-to-accept-ssl-connections
  }
});

var db = {}

// Models
db.tickets = require(__dirname + '/tickets')(sql, connection);
db.users = require(__dirname + '/users')(sql, connection);
db.roles = require(__dirname + '/roles')(sql, connection);
db.customers = require(__dirname + '/customers')(sql, connection);
db.customers_tickets = require(__dirname + '/customers_tickets')(sql, connection);

// Relationships
db.tickets.belongsToMany(db.customers, {
  through: db.customers_tickets,
  foreignKey: 'iticketid'
});
db.customers.belongsToMany(db.tickets, {
  through: db.customers_tickets,
  foreignKey: 'icustomerid'
});

module.exports = db;
