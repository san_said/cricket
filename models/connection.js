'use strict'

module.exports = (sequelize) => {

  var connection = new sequelize('Test_RedTicket', 'postgres', 'rabiosa8393', {
    host: 'localhost',
    port: 5433,
    dialect: 'postgres',
    define: {timestamps: false},
    pool: {
      max: 10,
      min: 5,
      idle: 10000
    },
    omitNull: true,
    dialectOptions: {
      ssl: false // reconfigure your psql server so that it accepts SSL - instructions: https://stackoverflow.com/questions/38134557/having-trouble-setting-up-postgres-server-to-accept-ssl-connections
    }
  });

  return connection;
}
