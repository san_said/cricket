'use strict'

module.exports = (sql, connection) => {

  var customerstickets = connection.define(
    'customers_tickets', {
      ientryid: {
        type: sql.BIGINT
      },
      icustomerid: {
        type: sql.INTEGER,
        primaryKey: true
      },
      iticketid: {
        type: sql.BIGINT,
        primaryKey: true
      }
    });

  return customerstickets;
}
