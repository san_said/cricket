'use strict'

module.exports = (sql, connection) => {

  var customers = connection.define(
    'customers', {
      icustomerid: {
        type: sql.INTEGER,
        primaryKey: true
      },
      scustomername: {
        type: sql.STRING(45),
        unique: true
      },
      scustomerdetails: {
        type: sql.TEXT,
        defaultValue: null
      }
    });

  return customers;
}
