'use strict'

module.exports = (sql, connection) => {

  var users = connection.define(
    'users', {
      iuserid: {
        type: sql.BIGINT,
        primaryKey: true
      },
      susername: {
        type: sql.STRING(25),
        unique: true
      },
      spassword: {
        type: sql.STRING(65)
      },
      sname: {
        type: sql.STRING(40)
      },
      suserdepartment: {
        type: sql.STRING(25)
      },
      suserrole: {
        type: sql.STRING(25)
      },
      semail: {
        type: sql.STRING(65),
        validate: {
          isEmail: true
        },
        defaultValue: null
      },
      snumber: {
        type: sql.STRING(20),
        defaultValue: null
      },
      sextension: {
        type: sql.STRING(10),
        defaultValue: null
      },
      suserdetails: {
        type: sql.TEXT,
        defaultValue: null
      }
    });

  return users;
}
