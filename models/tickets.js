'use strict'

module.exports = (sql, connection) => {

  var tickets = connection.define(
  'tickets', {
    iticketid: {
      type: sql.BIGINT,
      primaryKey: true
    },
    iticketref: {
      type: sql.BIGINT,
      unique: true
    },
    dtopened: {
      type: sql.DATE
    },
    stickettitle: {
      type: sql.STRING(80)
    },
    sticketdetails: {
      type: sql.TEXT,
      defaultValue: 'No description applied yet'
    },
    iprioritylevel: {
      type: sql.INTEGER,
      defaultValue: null
    },
    sassignedengineer: {
      type: sql.STRING(25)
    },
    slastupdate: {
      type: sql.TEXT,
      defaultValue: 'System update: ticket created!'
    },
    dtlastupdatetime: {
      type: sql.DATE
    },
    scurrentstatus: {
      type: sql.STRING(25),
      defaultValue: 'New'
    },
    dtlaststatuschange: {
      type: sql.DATE
    },
    dtticketcloseddate: {
      type: sql.DATE,
      defaultValue: null
    },
    sopenedby: {
      type: sql.STRING(25)
    }
  });

  return tickets;
}
