'use strict'

module.exports = (sql, connection) => {

  var roles = connection.define(
    'roles', {
      iroleid: {
        type: sql.INTEGER
      },
      srolename: {
        type: sql.STRING(25),
        primaryKey: true
      },
      sdepartmentname: {
        type: sql.STRING(25),
        primaryKey: true
      },
      badmin: {
        type: sql.BOOLEAN,
        defaultValue: false
      }
    });

  return roles;
}
