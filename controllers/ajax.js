'use strict'

var db = require(__dirname + '/../models/db');

// same controllers to be used for API?

module.exports.getContacts = (req,res) => {
  res.setHeader('Content-Type', 'text/plain')
  res.status(200).send({
    name: 'Sanyia Saidova',
    position: 'Lead engineer',
    email: 'ssa@contentguru.com',
    tel: '07907911106',
    comments: 'Lead engineer on UKPN'
  });
};

module.exports.addContacts = (req,res) => {
  console.log(util.inspect(req.body));

  var name = req.body.fullname;
  var email = req.body.email;
  var tel = req.body.telephone;

  db.Engineers
    .create({
      sengineertla: name,
      sengineermobile: tel,
      sengineeremail: email
    })
    .then( newrow => console.log(newrow.get({plain: true})) )

  res.end();
};

module.exports.updateTicket = (req, res) => {
  var ticketref = req.body.ticketref;
  var field = req.body.field;
  var data = req.body.data;

  if (field == 'ticket-title') {
    db.tickets
      .update({
        stickettitle: data
      },
      {
        where: {
          iticketref: ticketref
        }
      })
      .then(function() {
        res.status(200);
        res.end();
      })
      .catch(function () {
        res.send('Could not process request');
        res.end();
      });
  } else if (field == 'ticket-desc') {
    db.tickets
      .update({
        sticketdetails: data
      },
      {
        where: {
          iticketref: ticketref
        }
      })
      .then(function() {
        res.status(200);
        res.end();
      })
      .catch(function () {
        res.send('Could not process request');
        res.end();
      });
  }
};
