'use strict'

var db = require(__dirname + '/../models/db');

module.exports.getEngineers = (req, res) => {
  db.users
    .findAll({
      raw: true
    })
    .then( users => {
      res.render('engineers', {title: 'Engineers', allengineers: users});
      res.end();
    })
};

module.exports.getEngineerPage = (req, res) => {
  var engineertla = req.params.engineer;

  res.render('engineer', {title: engineertla, engineer: engineertla});
  res.end();
};
