'use strict'

module.exports.showHome = (req, res) => {
  // to add: if unauthorise, redirect to /login - use res.redirect() with cookies
  // to set a cookie, use req.session.{variable} = {value}; need to install
  // client-sessions before this can work
  // further instructions: https://stormpath.com/blog/everything-you-ever-wanted-to-know-about-node-dot-js-sessions
  res.render('main', {title: 'Main'});
  res.end();
};

module.exports.search = (req, res) => {
  var search = util.inspect(req.query)

  res.render('search', {title: 'Search Results', searchparams: search});
  res.end();
};

module.exports.showLogin = (req, res) => {
  // to add: if authorised, redirect to /
  res.render('login', {title: 'Login'});
  res.end();
};
