'use strict'

var db = require(__dirname + '/../models/db');

module.exports.getAdminPage = (req,res) => {
  // if authorised, redirect to /
  res.render('admin', {title: 'Admin'});
  res.end();
};
