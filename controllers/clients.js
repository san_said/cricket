'use strict'

var db = require(__dirname + '/../models/db');

module.exports.getClients = (req,res) => {
  res.render('clients', {title: 'Clients', allclients: 'none', listClients: true});
  res.end();
};

module.exports.getClientPage = (req,res) => {
  var clientname = req.params.client;
  var clientdescription;

  res.render('clients', {title: clientname, client: clientname, clientdescr: 'Test client description'});
  res.end();
};
