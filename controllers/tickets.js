'use strict'

var db = require(__dirname + '/../models/db');

module.exports.getTickets = (req, res) => {
  db.tickets
    .findAll({
      include: [{
        model: db.customers
      }]
    })
    .then(
      tickets =>  {
        var tickets_condensed = tickets.map( ticket => {
          return Object.assign({}, {
            iticketid: ticket.iticketid,
            iticketref: ticket.iticketref,
            stickettitle: ticket.stickettitle,
            dtopened: ticket.dtopened,
            iprioritylevel: ticket.iprioritylevel,
            bticketclosed: ticket.dtticketcloseddate,
            scurrentstatus: ticket.scurrentstatus,
            dtlastupdatetime: ticket.dtlastupdatetime,
            sassignedengineer: ticket.sassignedengineer,
            customers_affected: ticket.customers.map( customer => {
              return customer.scustomername
            })
          })
        });

        res.render('tickets', {title: 'Tickets', alltickets: tickets_condensed});
        res.end()
      }
    )
};

module.exports.getTicketPage = (req,res) => {
  var ticketref = req.params.ticket;

  db.tickets
    .findOne({
      where: {iticketref: ticketref},
      raw: true
    })
    .then( ticket => {
      if (ticket) {
        var stickettitle = ticket.stickettitle;
        var sticketdetails = ticket.sticketdetails

        res.render('ticket', {
          title: ticketref,
          tickettitle: stickettitle,
          ticketdesc: sticketdetails,
          ticket: ticketref
        });
        res.end();
      } else {
        res.render('ticket', {title: 'Ticket not found', ticket: ticketref, notFound: true});
        res.end()
      };
    })
    .catch( err => {
      res.render('tickets', {title: 'Ticket not found', ticket: ticketref, notFound: true});
      res.end()
    })
};

module.exports.createTicket =  (req,res) => {
  var title = req.body.ticketTitle; // redirect to ticket page if post successful - utlise promises? (.then(result => redirect, etc.))

  db.tickets
    .create({
      stickettitle: title,
      sassignedengineer: 'SSA',
      sopenedby: 'SSA'
    })
    .then( ticket => {
      res.redirect("http://localhost:8081/tickets/" + ticket.get({plain: true}).iticketref + "/");
      res.end();
    });

  // also send prompt to clear form
};
