'use strict'

// stores config related to front-end portion of app
 module.exports = {
  "menuItems": {
    "quickSelect": { // revise category name
      "miTickets": {
        "url": null,
        "icon": null,
        "description": "MI Tickets",
        "active": true
      },
      "calendar": {
        "url": null,
        "icon": null,
        "description": "Calendar",
        "active": true
      },
      "reports": {
        "url": null,
        "icon": null,
        "description": "Reports",
        "active": true
      },
      "monitoring": {
        "url": null,
        "icon": null,
        "description": "Monitoring",
        "active": true
      },
      "documentation": {
        "url": null,
        "icon": null,
        "description": "Topology",
        "active": false
      }
    },
    "main": {
      "home": {
        "url": null,
        "icon": null,
        "description": "Home",
        "active": true
      },
      "tickets": {
        "url": null,
        "icon": null,
        "description": "Tickets",
        "active": true
      },
      "clients": {
        "url": null,
        "icon": null,
        "description": "Clients",
        "active": true
      },
      "products": {
        "url": null,
        "icon": null,
        "description": "Products",
        "active": true
      },
      "systemInfo": {
        "url": null,
        "icon": null,
        "description": "System Information",
        "active": true
      }
    },
    "admin": {
      "users": {
        "url": null,
        "icon": null,
        "description": "Users",
        "active": true
      },
      "slas": {
        "url": null,
        "icon": null,
        "description": "SLAs",
        "active": true
      }
    }
  },
  "testing": true,
  "attachments": {
    "server": null,
    "path": "//",
    "credentials": {
      "username": null,
      "password": null
    },
  },
};
