'use strict'

// This config file is to allow easy modification to the menu items in RedTicket

// The reason for this is to encourage collaboration and to make it easier for
// engineers to add their own pages to RedTicket

// This config file acts as one large list. Each object in this list is a section with menu
// items containing the following attributes:
//    - name (string): this is the name of the menu item as display on the web append
//    - icon (string): path of the icon used with that menu item
//    - url (string): optional - this is the url that menu item redirects to. Will only redirect if hassubs is false
//    - description (string): this description will be used to describe what the function of the menu items
//    - active (bool): this can be toggled to hide or show a menu item from view (for example, if page is under construction)
//    - hassubs (bool): this can be toggled to change the function of the menu item. If true, will expand sub-menu items. If false, redirects to URL
//    - subs (list): optional - this is for menu items that will be displayed in this menu item when expanded. Will only display if hassubs is true

module.exports = [
  {
    section: "UI Elements",
    menuitems: [
      {
        active: true,
        name: "Components",
        icon: "components.jpg",
        description: "Components that can be used with this site",
        hassubs: true,
        url: "",
        subs: [
          {
            active: true,
            name: "Buttons",
            icon: "buttons.jpg",
            description: "List of all buttons that can be used with site",
            hassubs: false,
            url: "localhost:8081/buttons",
            subs: []
          },
          {
            active: true,
            name: "Badges",
            icon: "badges.jpg",
            description: "List of all badges that can be used with site",
            hassubs: false,
            url: "localhost:8081/badges",
            subs: []
          }
        ]
      },
      {
        active: true,
        name: "Tables",
        icon: "tables.jpg",
        description: "List of all tables",
        hassubs: false,
        url: "localhost:8081/tables",
        subs: []
      }
    ]
  },
  section: "Icons",
  menuitems: [
    {
      active: true,
      name: "Widgets",
      icon: "widgets.jpg",
      description: "List of all widgets",
      hassubs: false,
      url: "localhost:8081/widgets",
      subs: []
    }
  ]
]
