'use strict'

// stores backend related config

 module.exports = {
   istesting: true,
   testenv: {
     storageloc: {
       server: "test_server",
       path: "//"
       username: "admin",
       password: "admin"
     }
   },
   liveenv: {
     storageloc: {
       server: "live_server",
       path: "//",
       username: "admin",
       password: "admin"
     }
   }
};
