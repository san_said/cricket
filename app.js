'use strict'

var util = require('util')
  ,express = require('express')
  ,pug = require('pug')
  ,pg = require('pg')
  ,bodyParser = require('body-parser')
  ,config = require(__dirname + '/settings');

var app = express();

app.set('view engine','pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));

app.locals.authorisedEngineer = 'SSA'; // used as a variable in all pug templates - should turn into response locals (res.locals) which modifies variables for each request
app.locals.isAdmin = true;

// Import controllers
var mainController = require(__dirname + '/controllers/main');
var ticketsController = require(__dirname + '/controllers/tickets');
var engineersController = require(__dirname + '/controllers/engineers');
var clientsController = require(__dirname + '/controllers/clients');
var adminController = require(__dirname + '/controllers/admin');
var ajaxController = require(__dirname + '/controllers/ajax');

// Router - GET requests
app.get('/', mainController.showHome)
.get('/login', mainController.showLogin)
.get('/search', mainController.search)
.get('/tickets', ticketsController.getTickets)
.get('/tickets/:ticket/', ticketsController.getTicketPage)
.get('/engineers', engineersController.getEngineers)
.get('/engineers/:engineer', engineersController.getEngineerPage)
.get('/clients', clientsController.getClients)
.get('/clients/:client', clientsController.getClientPage)
.get('/admin', adminController.getAdminPage);

// Router - POST requests
app.post('/tickets/create', ticketsController.createTicket);

// Router - AJAX requests
app.get('/ajax/tickets/get-contacts', ajaxController.getContacts)
.post('/ajax/tickets/add-contact', ajaxController.addContacts)
.put('/ajax/tickets/update', ajaxController.updateTicket);

// API - TBC

// If page not found
app.use(function(req,res,next){
  res.setHeader('Content-Type', 'text/plain');
  res.status(404).send("Sorry, page can't be found!");
});

app.listen(8081);

console.log('Server running at http://127.0.0.1:8081');
